<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post`.
 */
class m180323_064638_create_tbl_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tbl_user', [
            'id' => $this->primaryKey(),
            'firstname' => $this->string()->notNull(),
            'lastname' => $this->string()->notNull(),
            'age' => $this->string()->notNull(),
            'sex' => $this->string(),
            'email' => $this->string(),
            'address' => $this->string(),
            'username' => $this->string()->notNull(),
            'password' => $this->string()->notNull(),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tbl_user');
    }
}
