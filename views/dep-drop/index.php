<?php
use app\models\Region;
use app\models\Province;
use app\models\District;
use kartik\depdrop\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'ภาค จังหวัด อำเภอ ';

//echo Html::getInputId($model, 'region_id');
//exit;   
?>
<h1><?=$this->title?></h1>
<?php $form = ActiveForm::begin()?>
<?php echo $form->field($model, 'region_id')->dropDownList(ArrayHelper::map(Region::find()->all(), 'id', 'name'), ['id' => 'ddl-region', 'prompt' => 'เลือกภาค'])?>
<?php echo $form->field($model, 'province_id')->widget(DepDrop::className(), [
    'options' => ['id' => 'ddl-province'],
    'data' => [],
    'pluginOptions' => [
        'depends' => ['ddl-region'],
        'placeholder' => 'เลือกจังหวัด',
        'url' => Url::to(['province-list'])
    ]
])?>
<?= $form->field($model, 'district_id')->widget(DepDrop::classname(), [
        'options' => ['id' => 'ddl-district'],
        'data' =>[],
        'pluginOptions'=>[
            'depends'=>['ddl-province'],
            'placeholder'=>'เลือกตำบล',
            'url'=>Url::to(['district-list'])
        ]
]); ?>

<?= Html::submitButton('ส่งข้อมูล', ['class' => 'btn btn-success'])?>
<?php ActiveForm::end()?>