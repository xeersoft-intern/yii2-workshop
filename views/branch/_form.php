<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Branch;
use app\models\Major;

/* @var $this yii\web\View */
/* @var $model app\models\Branch */
/* @var $form yii\widgets\ActiveForm */


echo $url_branch = Url::to(['branch/getbranch_major']);
?>

<div class="branch-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'branch_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'major_id')->textInput() ?>

    
    

   <?= $form->field($model,'major_id')->dropDownList(
       ArrayHelper::map(Major::find()->all(),'id','major_name'),
       ['prompt'=>'Select Major', 'onchange'=>"
            $.ajax({
                url: '$url_branch',
            type: 'get',
            data: {id: $(this).val()},
            success: function (data) {
                var txt_opt ='';
                $(data).each(function(key, val){
                    txt_opt +=  '<option value=\''+data[key].id+'\'>'+data[key].branch_name+'</option>'
               });

       
               
               $('#branch_name').html(txt_opt);
            }

        });
       "]
    ) ?>
    
    <?= $form->field($model,'branch_name')->dropDownList(
       ArrayHelper::map(Branch::find()->all(),'id','branch_name'),
       ['id'=>'branch_name', 'prompt'=>'Select Branch']
   ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
