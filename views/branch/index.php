<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Branch;
use app\models\Major;



/* @var $this yii\web\View */
/* @var $searchModel app\models\BranchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Branches';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="branch-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Branch', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [         
            
            
            [
                'attribute'=> 'major_id',
                'value'=> 'major.major_name',
                //'filter'=> Html::activeDropDownList($searchModel,'major_id',ArrayHelper::map(Major::find()->asArray()->all(),'id','major_name'),['class'=>'form-control','prompt'=>'Pleaseasdasd'])
                'filter'=>array("1"=>"IT","2"=>"Business","3"=>"นิเทศ","4"=>"แพทย์")

            ],
            
            
            'branch_name',

                            
            

            
            
        ],
        
    ]);
    
    
    ?>
    

    
    
</div>
