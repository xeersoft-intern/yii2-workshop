<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;



/* @var $this yii\web\View */
/* @var $searchModel app\models\TblUserSreach */
/* @var $dataProvider yii\data\ActiveDataProvider */

/*foreach ($models as $model)
echo $model->post_title.'<br>';

echo LinkPager::widget([
    'pagination' => $pages,
    ]);*/

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'firstname',
            'lastname',
            'age',
            'sex',
            //'email:email',
            //'address',
            'username',
            'password',

            ['class' => 'yii\grid\ActionColumn'],
        ],
       
    ]);
  
    ?>

   

   
</div>

