<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "major".
 *
 * @property int $id
 * @property string $major_name
 *
 * @property Branch[] $branches
 */
class Major extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'major';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['major_name'], 'required'],
            [['major_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'major_name' => 'Major Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranches()
    {
        return $this->hasMany(Branch::className(), ['major_id' => 'id']);
    }
}
