<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;

use kartik\widgets\DepDrop;

use app\models\Branch;

use app\models\Major;

/**
 * This is the model class for table "branch".
 *
 * @property int $id
 * @property string $branch_name
 * @property int $major_id
 *
 * @property Major $major
 */
class Branch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'branch';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['branch_name', 'major_id'], 'required'],
            [['major_id'], 'integer'],
            [['branch_name','major_id'], 'string', 'max' => 255],
            [['major_id'], 'exist', 'skipOnError' => true, 'targetClass' => Major::className(), 'targetAttribute' => ['major_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'branch_name' => 'Branch ',
            'major_id' => 'Major',
        ];
    }

    /*public function getCompany(){
        return $this->hasOne(Major::className(),['id','major_id']);
    }*///เพื่อไว้

    public function getBranch(){
        return $this->hasOne(Branch::className(),['id'=>'branch_name']);
    }
   
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMajor()
    {
        return $this->hasOne(Major::className(), ['id' => 'major_id']);
    }
}
