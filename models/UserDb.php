<?php

namespace app\models;




class UserDb extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
  
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'age', 'email', 'address', 'username', 'password'], 'required'],
        ];
    }

    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'age' => 'Age',
            'sex' => 'Sex',
            'email' => 'Email',
            'address' => 'Address',
            'username' => 'Username',
            'password' => 'Password',
        ];
    }


    
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }


   
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }


    
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

 

    public function getId()
    {
         return $this->getPrimaryKey();
    }


    
    public function getAuthKey()
    {
        return $this->authKey;
    }


    
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    

    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
