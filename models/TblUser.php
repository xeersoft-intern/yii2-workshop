<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_user".
 *
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property int $age
 * @property string $sex
 * @property string $email
 * @property string $address
 * @property string $username
 * @property string $password
 */
class TblUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'age', 'email', 'address', 'username', 'password'], 'required'],
            [['age'], 'integer'],
            [['firstname', 'lastname', 'sex', 'email', 'address', 'username', 'password'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'age' => 'Age',
            'sex' => 'Sex',
            'email' => 'Email',
            'address' => 'Address',
            'username' => 'Username',
            'password' => 'Password',
        ];
    }
}
