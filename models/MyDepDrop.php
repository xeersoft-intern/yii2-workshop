<?php


namespace app\models;


use yii\base\Model;

class MyDepDrop extends Model
{
    public $region_id;
    public $province_id;
    public $district_id;

    public function rules()
    {
        return [
            [['region_id', 'province_id', 'district_id'], 'required']];
    }

    public function attributeLabels()
    {
        return [
            'region_id' => 'ภาค',
            'province_id' => 'จังหวัด',
            'district_id' => 'อำเภอ',
        ];
    }
}